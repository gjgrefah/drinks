import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IngredientsService {
  private API_SERVER = "https://www.thecocktaildb.com/api/json/v1/1/search.php?f=a";
  
  constructor(public http:HttpClient){}

  public getIngredient():Observable<any>
  {
    return this.http.get(this.API_SERVER);
  }
}
