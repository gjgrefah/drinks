import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DrinkComponent } from './components/drink/drink.component';
import { IngredientsComponent } from './components/ingredients/ingredients.component';

const routes: Routes = [

  { path : '', component:DrinkComponent},
  { path : 'drink', component:DrinkComponent  },
  { path : 'drink/:id', component:IngredientsComponent },
  { path : 'ingredients', component:IngredientsComponent },
  { path : '**', redirectTo:"/drink"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
