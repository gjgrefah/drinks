import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DrinkService } from './service/drink.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  drinks:any;

  constructor(public drink:DrinkService, private router:Router){}

  ngOnInit(){
    this.drink.getDrink().subscribe
    (
      (r) => {this.drinks = r.drinks; console.log(r)},
      (e) => {console.error(e)}
    )
  }

  goDetail(id:string){
    console.log(id)
    this.router.navigate(["/drink", id])
  }

}
