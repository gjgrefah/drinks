import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DrinkComponent } from './components/drink/drink.component';
import { IngredientsComponent } from './components/ingredients/ingredients.component';
import { DrinkService } from './service/drink.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    DrinkComponent,
    IngredientsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    DrinkService
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
