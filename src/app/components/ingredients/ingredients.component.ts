import { Component, OnInit } from '@angular/core';
import { IngredientsService } from 'src/app/service/ingredients.service';
import { Router } from '@angular/router';
import { DrinkService } from 'src/app/service/drink.service';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent implements OnInit {

  ingredients: any;

  constructor(public ingredient: IngredientsService, private router: Router, public drink: DrinkService) { }

  ngOnInit(): void {
    this.ingredient.getIngredient().subscribe
      (
        (r) => { this.ingredients = r.ingredients; console.log(r) },
        (e) => { console.error(e) }
      )
  }
  goBack(){
    this.router.navigate(["/drink"])
  }
}

