import { Component, OnInit } from '@angular/core';
import { DrinkService } from 'src/app/service/drink.service';
import { Router } from '@angular/router';
import { IngredientsService } from 'src/app/service/ingredients.service';


@Component({
  selector: 'app-drink',
  templateUrl: './drink.component.html',
  styleUrls: ['./drink.component.css']
})
export class DrinkComponent implements OnInit {

  drinks:any;

  constructor(public drink:DrinkService, private router:Router, public ingredient:IngredientsService){}

  ngOnInit(){
    this.drink.getDrink().subscribe
    (
      (r) => {this.drinks = r.drinks; console.log(r)},
      (e) => {console.error(e)}
    )
  }

  goDetail(id:string){
    console.log(id)
    this.router.navigate(["/drink", id])
  }

}

