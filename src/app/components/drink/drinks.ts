export class Drink {
    public strDrink: String = '';
    public strCategory: String = '';
    public strAlcoholic: String = '';
    public strGlass: String = '';
    public strDrinkThumb: String = '';
    public strIngredient1: String = '';
    public strIngredient2: String = '';
    public strIngredient3: String = '';
    public strIngredient4: String = '';
    
}

